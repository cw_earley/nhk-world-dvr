#!/bin/python
'''
This is a ultility script for users wishing to remove shows from the recorded database.
Usually to force the DVR to re-record a show.

Usage: $ python [-v] removeshows.py seriesId:airingId seriesId:airingId ...
        -v : Print out all seriesId:airingId in the database to console.

Note: You can get a list of show:episode entries the DVR has recently recorded using the command
tail -n 50 /path/to/the/NHKDVR.log | grep "New show"

From there, you can just copy-paste the seriesId:airingId entries into your removeshows command.

'''
import sys
import pickle
# http://stackoverflow.com/questions/714063/importing-modules-from-parent-folder
from pathlib import Path
sys.path.append(str(Path('.').absolute().parent))
import nhksettings

if len(sys.argv) < 2:
    print("Usage: $ python remove_shows.py seriesId:airingId seriesId:airingId ...")
    exit()

# Chop off the command (argv[0]) from the list of arguments to isolate the
# list of shows
to_remove = sys.argv[1:]

print("Opening database...")
with open(nhksettings.DB_NAME, 'rb') as f:
    seen_shows = pickle.load(f)

if to_remove[0] == '-v':
    for i in seen_shows:
        print(i)
    exit()

print("Removing given shows from database...")
# create a new list of shows leaving out the shows to be removed
seen_shows = [s for s in seen_shows if s not in to_remove]

print("Writing new database to file...")
with open(nhksettings.DB_NAME, 'wb') as f:
    pickle.dump(seen_shows, f)
print("Done!")
exit()
