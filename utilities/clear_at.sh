#!/bin/bash
: '
 This is just a little utility script that removes all scheduled jobs from the at
 index. Sometimes things just go wrong and you want to start again from a blank slate.

 Usage: ./clear_at.sh
'
for i in `atq | awk '{print $1}'`;do atrm $i;done
