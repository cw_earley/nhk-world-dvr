# NHKDVR Utilities

This is a collection of short python and bash scripts that provide functions that users might find occasionally useful in managing their NHK DVR instance.

### getShowIDs.py

This is a simple helper script used to generate the comprehensive list of NHK World shows
and their respective show IDs. Each time this script is executed, a json document of all
available show data from NHK for 90 days is requested and parsed into an alphabetized document.
Shows that are already listed are tracked in a simple pickle database to 
prevent duplicates.

      Usage: python getshowIDs.py /path/to/where/you/want/the/showlist.txt

Note: This script is best run as a weekly cron job like so - 
\@weekly python getshowIDs.py /path/to/where/you/want/the/showlist.txt

### removeshows.py

This is a utility script for users wishing to remove shows from the recorded database.
Usually to force the DVR to re-record a show.

    Usage: python [-v] removeshows.py seriesId:airingId seriesId:airingId ...
           -v : Print out all seriesId:airingId in the database to console.

Note: You can get a list of show:episode entries the DVR has recently recorded using the command
tail -n 50 /path/to/the/NHKDVR.log | grep "New show"

From there, you can just copy-paste the seriesId:airingId entries into your removeshows command.

### libraryconverter.py

This is a utility script for users wishing to migrate from the previous
NHK DVR library directory setup to the newer version.

    Usage: python libraryconverter.py /path/to/NHK/video/dir/

Once run, this script will:

1.  Go through the NHK directory and move all the loose
    videos into directories with their show title as the name.

2.  Go through the NHK directory and rename all the old style
    files to the new filename spec.
    Old: Sumopedia_1155_07-24-2016.mp4
    New: Sumopedia - 07-24-2016.mp4

### clear_at.sh

 This is just a little utility script that removes all scheduled jobs from the at
 index. Sometimes things just go wrong and you want to start again from a blank slate.

     Usage: ./clear_at.sh