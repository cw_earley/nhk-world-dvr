#!/bin/python
"""
  This is a simple helper script used to generate the comprehensive list of NHK World shows
  and their respective show IDs. Each time this script is executed, a json document of all
  available showdata from NHK for 90 days is requested and parsed into an alphabetized document.
  Shows that are already listed are tracked in a simple pickle database to 
  prevent duplicates.

  Usage: python getshowIDs.py /path/to/where/you/want/the/showlist.txt

  Note: This script is best run as a weekly cron job.

"""

import requests
import pickle
import time
import sys


def main():
    numargs = len(sys.argv)
    if not numargs == 2:
        print("Usage: python getshowIDs.py /path/to/where/you/want/the/showlist.txt")
        exit()

    listpath = sys.argv[1]

    nhk_json = 'http://api.nhk.or.jp/nhkworld/epg/v3/world/s%d-e%d.json?apikey=EJfK8jdS57GqlupFgAfAAwr573q01y6k'

    curr_time = int(time.time() * 1000)

    # get all shows for a 90 day period
    r = requests.get(nhk_json % (curr_time, curr_time + 7776000000))

    show_list = r.json()['channel']['item']

    try:
        with open('showIDs.db', 'rb') as f:
            indexed_shows = pickle.load(f)
    except IOError:
        indexed_shows = []
    except EOFError:
        indexed_shows = []

    for i in show_list:
        # http://stackoverflow.com/a/2258273
        t = (i['seriesId'], i['title'])
        if t not in indexed_shows:
            indexed_shows.append(t)
    # sort the shows, disregard case since NHK likes all lowercase titles
    indexed_shows = sorted(indexed_shows, key=lambda x: x[1].lower())

    with open('showIDs.db', 'wb') as f:
        pickle.dump(indexed_shows, f)

    with open(listpath, 'w') as f:
        for i in indexed_shows:
            f.write("%s" % i[0] + '\t' + i[1] + '\n')

if __name__ == '__main__':
    main()
