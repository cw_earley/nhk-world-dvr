#!/bin/python3

'''
This is a ultility script for users wishing to migrate from the previous
NHK DVR library setup to the newer version.

Usage: python3 libraryconverter.py /path/to/NHK/video/dir/

Once run, this script will:

1.  Go through the NHK directory and move all the loose
    videos into directories with their show title as the name.

2.  Go through the NHK directory and rename all the old style
    files to the new filename spec.
    Old: Sumopedia_1155_07-24-2016.mp4
    New: Sumopedia - 07-24-2016.mp4

'''

from os import scandir
from os import mkdir
from os import walk
from os import rename
from os.path import join
from os.path import splitext
from os.path import isdir
import sys
from shutil import move


def get_index(s, char):
    '''
    Get the index of the first occurance of character char in the string s
    '''
    if char in s:
        for i, c in enumerate(s):
            if c == char:
                return i
    return None


def get_name(s):
    '''
    Return the subset of string s before the first underline character
    '''
    name_boundry = get_index(s, '_')

    if name_boundry:
        return s[:name_boundry]
    else:
        return None


def main():

    numargs = len(sys.argv)
    if not numargs == 2:
        print("Usage: python3 libraryconverter.py /path/to/NHK/video/dir/")
        exit()

    # Get the second arg, first arg is the script name.
    NHKDIR = sys.argv[1]

    print("#### Step one: Organize files into named subdirectories...")
    for entry in scandir(NHKDIR):
        if not entry.name.startswith('.') and entry.is_file():
            # make sure this is a legit videofile
            if entry.name.count('_') == 2 and '.mp4' in entry.name:
                # extract the name from the old filename style
                # by taking all the characters before the first underscore
                show_name = get_name(entry.name)
                folder_path = join(NHKDIR, show_name)
                if not isdir(folder_path):
                    mkdir(folder_path)
                print("Moving " + entry.name + " to " + folder_path)
                move(entry.path, folder_path)
            else:
                print("Skipping " + entry.name)

    print("#### Step two: Update file names...")
    for root, _, files in walk(NHKDIR):
        for name in files:
            # make sure this is a legit videofile
            if name.count('_') == 2 and '.mp4' in name:
                # strip off the extension
                rawname = splitext(name)[0]
                # split the name into the parts we want to shuffle about
                nameparts = rawname.split('_')
                # assemble the new name in plex-ready format
                newname = nameparts[0] + ' - ' + \
                    nameparts[2] + '.mp4'
                # rename the files in place
                print('Renamed ' + join(root, name) +
                      ' to ' + join(root, newname))
                rename(join(root, name), join(root, newname))
            else:
                print("Skipping " + name + " ...")
    print("### Finished library migration!")

if __name__ == "__main__":
    main()
