# NHK World DVR

## Synopsis

NHK World DVR is a simple python script for automatically recording programming from the
online streaming service NHK World.

*This is an unofficial fan work for __personal use only__. This project is in no way
affiliated with NHK, NHK World, Japan International Broadcasting, Inc. or any other entities
therein.*

## Features

- Blacklist: Don't record the shows that you're not interested in.
- Duplicate filter: A simple database keeps tabs on what shows you've already archived to
prevent recording the same episodes again and again.
- Rock solid timing: Leverages the tried-and-true unix `at` job scheduling daemon for
launching and stopping commands.

## Motivation

NHK World is a 24/7 TV channel broadcasting around the world on numerous services from pay
cable, hi-def satellite, to most importantly free HLS streaming online. I've been a fan of
NHK's programming for years but finding time to sit down and watch the shows that interest me
on the live stream has become nearly impossible. After some labor intensive experiments using
Windows Task Scheduling to record shows, a more comprehensive and hands-off approach was
sorely needed.

Since I already have an Arch server running Plex stashed away, creating a simple daemon to
archive shows right off of the stream made perfect sense. After some [research on Python
job/process scheduling](http://simeonfranklin.com/blog/2012/aug/14/scheduling-tasks-python/) I
decided to forego any staight-Python solution to instead leverage the vetted unix `at` service
to manage ffmpeg recording. This has its limitations (The minute accuracy limit comes to mind)
but over the course of development, I have been able to make a script that works well enough.

## Requirements

- A unix-based operating system - This script relies on the unix shell and standard services
so windows support isn't really in the cards.
- Python 3.3+
- [Python Requests](http://docs.python-requests.org/en/latest/) - To download json info from NHK's
schedule page
- [livestreamer](https://github.com/chrippa/livestreamer) - For actually recording the HLS stream. Try to get the freshest version you can.
- [FFmpeg](http://ffmpeg.org/) - For adding metadata to the recorded show files.
- [atd](https://en.wikipedia.org/wiki/At_%28Unix%29) - Usually included with modern unix distros, be sure to activate the daemon.

## Installation

 1. Fulfill all the dependencies
 2. Edit any relevant settings in nhksettings.py to match your server setup. Namely VIDEO_DIR, BLACKLIST and DB_NAME.
 3. Edit the username and project path variables in schedule_job.sh
 4. Add a daily cron job that runs schedule_job.sh. I personally use `00 00 * * *     
/path/to/schedule_jobs.sh &`
 5. Come back in a day and see all the awesome NHK content waiting to be watched!

## Utilities
Located in the utilities subfolder is a collection of small scripts that may prove useful for NHKDVR users.

-  getShowIDs.py - Generate a comprehensive list of NHK World shows
and their respective show IDs.
-  removeshows.py - Remove shows from the recorded database.
-  libraryconverter.py - Migrate from the previous
NHK DVR library directory setup to the newer version.
-  clear_at.sh - Remove all scheduled jobs from the at
 index.

See the README located in that directory for more information on usage.



## Tips
- *Play around with the START_OFFSET value in nhksettings.py.* Depending on your distance from
the NHK stream servers the latency will vary so you'll need to change things. Be aware that
NHK's start times are rather early (About 30 seconds too early for me) so you might end up
recording the last few seconds of the previous program if the offset is too low.

## To do
 - Remove any hardcoded directories to make things as generic and easy as possible.
 - Make database editing easier.
 - Remove the need for the schedule_job.sh wrapper script

## Contributing

Feel free to fork, pull, suggest improvements, and report any bugs.
If you require any help getting the system installed and working, feel free to message me via
bitbucket mail.

## License

The MIT License (MIT)

Copyright (c) 2015 Christopher Earley

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.