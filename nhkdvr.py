#!/bin/python3

import pickle
import requests
import time
import nhksettings
import logging
import re
from subprocess import Popen, PIPE
from os import devnull
from os import path
from os import mkdir
from os import urandom
from os.path import isdir
from shlex import quote


def create_job(s):
    """
        Uses the given show to generate an $ at jobs to record that program
        s is the list of dicts containing all the data for an NHK show
    """

    # get the start and end times in terms of seconds
    start_time = (float(s['pubDate']) / 1000)
    end_time = (float(s['endDate']) / 1000)

    metadata = create_metadata(s['title'], s['subtitle'],
                               s['description'], s['airingId'])

    # I decided to use the HHMM mm/dd/YYYY time format for $ at
    at_cmd_s = ['at', time.strftime("%H%M %m/%d/%Y",
                                    time.localtime(start_time))]
    at_cmd_e = ['at', time.strftime("%H%M %m/%d/%Y",
                                    time.localtime(end_time))]

    # Unix filename friendly timestamp
    date = time.strftime("%m-%d-%Y", time.localtime(start_time))
    # More sanitizing. Need to first remove non-valid characters
    filename = "".join([c for c in s['title']
                        if re.match(r'\w', c) or c == ' '])
    # replace spaces with periods and form into final filename
    filename = filename.replace(" ", ".")
    # create the abs path for this show with its own show directory
    filedir = nhksettings.VIDEO_DIR + filename + '/'
    # create a directory for the show if it doesn't exist
    if isdir(filedir):
        logging.debug(filename + ' directory exists!')
    else:
        logging.debug('Creating show directory: ' + filedir)
        mkdir(filedir)
    # e.g. /path/to/Her.Story/Her.Story - 09-29-2015 - 021.mp4
    fullfilename = filename + ' - ' + \
        date + ' - ' + s['airingId'] + '.mp4'
    # for extra insurance, check it using shlex's quote
    finalfilepath = quote(filedir + fullfilename)
    downloadpath = quote(nhksettings.DOWNLOAD_DIR + fullfilename)

    # create a random bit of text to remove any chance of file collision
    randomappend = str(int.from_bytes(urandom(3), byteorder="big"))

    # The abs path to the temp file used for adding metadata
    tempfile = nhksettings.DOWNLOAD_DIR + filename + '-' + randomappend + '.mp4'

    useragent = '--http-header "User-Agent"="Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"'

    # To get second accuracy we have to combine at and sleep.
    # at gets us to the minute we need, sleep adjusts for the seconds.
    # It's a terrible hack, I know. :\
    livestreamer_cmd = 'sleep {offset} ; livestreamer {a} -l info hlsvariant://{u} best --retry-streams 5 --retry-open 10 --hls-live-edge 9 --hls-segment-attempts 4 --hls-segment-timeout 60 --hls-timeout 120 --hls-segment-threads 2 -o {f} >> livestreamer.log'.format(
        offset=nhksettings.START_OFFSET, u=nhksettings.STREAM_URL, f=downloadpath, a=useragent)

    # Since livestreamer wasn't focused on file output, it has no method of
    # embedding metadata. ffmpeg, on the other hand, excels at it. If only
    # it could handle recording HLS streams too...
    ffmpeg_cmd_tmp = 'ffmpeg -y -i {f} -hide_banner -loglevel quiet -copy_unknown -acodec copy -vcodec copy {m} -strict -2 -bsf:a aac_adtstoasc {t}'.format(
        f=downloadpath, t=tempfile, m=metadata)

    ffmpeg_cmd_final = 'ffmpeg -y -i {t} -hide_banner -loglevel quiet -copy_unknown -acodec copy -vcodec copy -strict -2 -bsf:a aac_adtstoasc {f}'.format(
        f=finalfilepath, t=tempfile)

    # delete the temporary file and download file
    remove_cmd = 'rm ' + tempfile + ' ' + downloadpath

    logging.debug("Final livestreamer CMD : " + livestreamer_cmd)
    logging.debug('Final ffmpeg CMD : ' + ffmpeg_cmd_tmp +
                  ' && ' + ffmpeg_cmd_final)

    # Start the recording
    transmit_at_cmd(at_cmd_s, livestreamer_cmd)
    # End the recording by killing the process after a little wait period
    # The -o specifies pkill to kill the oldest processs by that name.
    # This will ensure the right lser will get killed in the event of overlap.
    # Give livestreamer time to end the recording before modifying the file's
    # metadata
    transmit_at_cmd(at_cmd_e, 'sleep {offset} ; pkill -o -f livestreamer ; sleep 15 ; {ft} ; {ff} ; {rm}'.format(
        offset=nhksettings.END_OFFSET, ft=ffmpeg_cmd_tmp, ff=ffmpeg_cmd_final, rm=remove_cmd))


def transmit_at_cmd(at_cmd, payload_cmd):
    """
    Use the python subprocess library to launch at job commands
    with a given payload
    """
    p = Popen(at_cmd, universal_newlines=True, stdin=PIPE,
              stdout=open(devnull, 'wb'), stderr=open(devnull, 'wb'))
    p.communicate(payload_cmd)


def create_metadata(show_title, episode_title, description, episode_id, network='NHK'):
    """
        Create a formatted string containing the ffmpeg arguments to add
        relevent file metadata to the final mp4 video
    """
    # Need to sanitize some fields a bit since NHK likes to nest quotes
    # This python 3.3+ function turns strings into shell-friendly quotes
    description = quote(description)
    show_title = quote(show_title)
    episode_title = quote(episode_title)
    # make all the data into a list of tuples for easy comprehension
    data = [("show", show_title), ("title", episode_title), ("description", description),
            ("episode_id", episode_id), ('network', network)]

    # combine all the metadata entries into one string
    return " ".join(["-metadata " + i[0] + "=" + i[1] for i in data])


def write_jobs(show_list):
    """
        Uses the given list of programs to generate multiple at jobs to record
        interesting programs
    """

    # Try to open and load the show list from the pickle file object
    try:
        with open(nhksettings.DB_NAME, 'rb') as f:
            seen_shows = pickle.load(f)
    except IOError:
        # The file cannot be opened, or does not exist.
        # Initialize your settings as defaults and create a new database file.
        seen_shows = []
    except EOFError:
        # The file is created, but empty so write new database to it.
        seen_shows = []

    for s in show_list:
        # Using the airing/series ids should be enough to keys that are unique
        # to each episode. This can be changed later to include other show info
        key = s['seriesId'] + ':' + s["airingId"]
        # New episode? Record it and add it to the list of old stuff.
        if key not in seen_shows:
            create_job(s)
            seen_shows.append(key)
            logging.debug('New show : {t} {s}:{a}'.format(t=s['title'],
                                                          s=s['seriesId'], a=s["airingId"]))
        else:
            logging.debug('Dup, skipping: {t} {s}:{a}'.format(t=s['title'],
                                                              s=s['seriesId'], a=s["airingId"]))

    with open(nhksettings.DB_NAME, 'wb') as f:
        pickle.dump(seen_shows, f)


def get_schedule(json_url, blacklist):
    """
        Retrives the json schedule from the NHK site for the current day and
        returns a filtered list
    """

    r = requests.get(json_url)

    # strip off the excess wrapper keys from the json dict to get a list of all
    # the shows where each list item is a list of dicts containing show info
    show_list = r.json()['channel']['item']

    # remove duplicate shows - http://is.gd/J4xDtB
    cleaned_list = []
    seen = set()
    for i in show_list:
        # create tuples with only the relevent data so we can check for dupes
        # since dict objects cannot be hashed for comparisons
        t = (i['seriesId'], i['airingId'])
        if t not in seen:
            seen.add(t)
            # add the unique dict to the cleaned list
            cleaned_list.append(i)

    # mutate show list by removing items from blacklist
    cleaned_list[:] = [i for i in cleaned_list
                       if i["seriesId"] not in blacklist]

    return cleaned_list


def main():
    """
        Read the NHK world json schedule and create $at jobs to record
        new shows while populating a database to check for dupes
    """

    log_file_dir = path.join(path.dirname(path.abspath(__file__)),
                             nhksettings.LOG_NAME)
    logging.basicConfig(filename=log_file_dir,
                        level=logging.getLevelName(nhksettings.LOG_LVL),
                        format='%(asctime)s %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    logging.info('NHK DVR Starting...')

    # The current time since the epoch in miliseconds, offset by 24 hours.
    # We want tomorrow's shows, yo.
    # 86400 seconds in a day
    curr_time = int((time.time() + 86400) * 1000)
    # curr_time = int((time.time() + 1800) * 1000)

    # The NHK API url to get a json file containing tomorrow's shows.
    # The time duration between s and e can be lengthened without issue
    # but ~24 hours is enough
    json_url = nhksettings.JSON_URL % (curr_time,
                                       curr_time + nhksettings.DAY_OFFSET)
    logging.debug("Current JSON URL: " + json_url)
    shows = get_schedule(json_url, nhksettings.BLACKLIST)
    write_jobs(shows)
    logging.info('NHK DVR Finished!')
    exit()


if __name__ == "__main__":
    main()
