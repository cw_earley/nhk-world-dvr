"""
    settings and constants for the NHK DVR
"""
# Number of miliseconds from start to end of the json schedule period
# This duration is what the official NHK world site uses to populate
# the schedule page
# The default start and end times from NHK's site are:
# Start: 05:00:00 GMT (12:00:00 AM GMT 5:00 DST)
# End:	04:59:00 GMT (11:59:00 PM GMT 5:00 DST) the following day
# Time difference in miliseconds for this period: 86,340,000
DAY_OFFSET = 86340000
# Start time correction for given pubDates in seconds
# From initial testing it seems pubDate starts 45-55 seconds too early
START_OFFSET = 60
# End time correction for given pubDates in seconds
# Useful to correct for any issues that may cut off the end of a show
END_OFFSET = 20
# Global path to where the in-progress recordings should be kept.
DOWNLOAD_DIR = '/path/to/downloads/dir/'
# Global path to where the final recordings should reside
# Make sure to have a trailing forward slash on the end!
VIDEO_DIR = '/path/to/media/storage/'
# HLS stream URL
STREAM_URL = 'https://nhkworld.webcdn.stream.ne.jp/www11/nhkworld-tv/global/263941/live_wa_s.m3u8'
# This is a python list of all the show IDs that you DO NOT want to record.
# Show IDs for programs are listed in.
# Check out the NHKShowList.txt for a list of all the active NHK shows and
# their IDs
BLACKLIST = ['2058', '6207', '1001']
# This link was scraped from the NHK World schedule page, s%d and e%d are to be
# replaced with the ephoc times
# to specify the scheule duration to be returned (miliseconds since epoch)
JSON_URL = 'https://api.nhk.or.jp/nhkworld/epg/v7/world/s%d-e%d.json?apikey=EJfK8jdS57GqlupFgAfAAwr573q01y6k'
# Name of the pickle database used to track what shows have been recorded
DB_NAME = "/path/to/NHKDVR.DB"
# Logging stuff
LOG_NAME = 'NHKDVR.log'
LOG_LVL = "DEBUG"
