#!/bin/sh
# Due to some limitations with ffmpeg's report option and at's generated scripts this simple 
# wrapper script is used to force all logs generated into the project directory.
umask 22 
PATH=/usr/bin:/bin; export PATH


# Change these variables to match your setup
USER=chris; export USER
# This is the full path to the NHK DVR files
HOME=/home/chris/nhk-world-dvr; export HOME 

cd $HOME || {
         echo 'Execution directory inaccessible' >&2
         exit 1
}

python3 $HOME/nhkdvr.py
